const base = "http://pmcglinchey06.lampt.eeecs.qub.ac.uk/gaea/api";
//const base = "https://youthful-gates-97d844.netlify.app/api";

const endpoints = {
  "addCSV": base + "/addData/csv.php",
  "addUser": base + "/addUser.php",
  "authenticate": base + "/authenticate.php",
  "getYears": base + "/years.php",
  "getGraphData": base + "/getGraphData.php",
  "addContent": base + "/addData/addContent.php",
  "getLog": base + "/getData/getLog.php",
  "getContent": base + "/getData/getContent.php",
  "getArticle": base + "/getData/getArticle.php",
  "deleteArticle": base + "/deleteData/deleteArticle.php",
  "getLineData": base + "/getData/getLineData.php",
  "getCategoricalCounts": base + "/getData/getCategoricalCounts.php"
}

export default endpoints;