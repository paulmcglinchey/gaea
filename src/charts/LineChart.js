import React from 'react';
import { Line } from 'react-chartjs-2';


const MultiAxisLine = (props) => {
  const data = {
    labels: props.labels,
    datasets: props.datasets
  };

  const options = {
    scales: {
      yAxes: {
        display: false
      }
    },
  };
  return (
    <div>
      <Line data={data} options={options} />
    </div>
  )
}

export default MultiAxisLine;