import React from 'react';
import { Scatter } from 'react-chartjs-2';

const TestChart = props => {

  const data = {
    datasets: [
      {
        label: props.label,
        data: props.data,
        backgroundColor: 'rgba(255, 99, 132, 1)',
      },
    ],
  };

  const options = {
    scales: {
      yAxes: [
        {
          ticks: {
            beginAtZero: true,
          },
        },
      ],
    },
  };

  return (
    <Scatter data={data} options={options} />
  )

}

export default TestChart;