import { Fragment, useState } from "react";
import AdminSubmitButton from './AdminSubmitButton.js';
import schema from '../../json/csvInputSchema.json';
import TogglerButton from "./TogglerButton.js";
import CSVFieldButton from "./CSVFieldButton.js";
import SpinnerSVG from "../svg/SpinnerSVG.js";
import endpoints from '../../endpoints.js';

const CSVUploader = (props) => {

  const [file, setFile] = useState(null);
  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [fileError, setFileError] = useState(null);
  const [dataError, setDataError] = useState(null);
  const [message, setMessage] = useState(null);
  const [viewDataTypes, setViewDataTypes] = useState(false);

  const viewDataTypesToggler = () => setViewDataTypes(!viewDataTypes);

  const onSubmit = () => {
    setFileError(null);
    setDataError(null);
    setMessage(null);
    setError(null);
    setIsLoading(true);

    if (!file) {
      setError("File must not be null");
      setIsLoading(false);
      return;
    }

    console.log(file);

    const formData = new FormData();
    formData.append('file', file);
    formData.append('username', props.loggedInUser);

    fetch(endpoints.addCSV, {
      method: 'POST',
      headers: {},
      body: formData
    })
      .then(res => res.json())
      .then(
        (result) => {
          if (result.fileError) {
            setFileError(result.fileError);
          } else if (result.dataError) {
            setDataError(result.dataError);
          } else {
            setMessage(result.message);
          }
          setIsLoading(false);
        }
      )
      .catch(
        (error) => {
          setIsLoading(false);
          console.log(error);
        }
      )
  }

  return (
    <Fragment>
      <div className="mb-3">
        <h1 className="font-bold text-xl">Expected Format</h1>
        <p>CSV file must be uploaded with the following specification, using commas as separators.</p>
        <div className="border-2 border-emerald-600 p-2 rounded">
          <div className="flex flex-wrap">
            {viewDataTypes && schema.map((item, key) => {
              return (
                <div key={key} className="flex p-1 items-center">
                  <CSVFieldButton title={item.title} type={item.type} showType={viewDataTypes} />
                  <div className="p-1 font-bold">{key < (schema.length - 1) && ","}</div>
                </div>
              )
            })}
            {!viewDataTypes && schema.map((item, key) => {
              return (
                <div key={key} className="flex p-1 items-center">
                  <CSVFieldButton title={item.title} type={item.type} showType={viewDataTypes} />
                  <div className="p-1 font-bold">{key < (schema.length - 1) && ","}</div>
                </div>
              )
            })}
          </div>
          <div className="flex p-1 justify-end">
            <div>
              <TogglerButton title="Toggle data types" onclick={viewDataTypesToggler} />
            </div>
          </div>
        </div>
      </div>
      <form className="w-full" onSubmit={(e) => {
        e.preventDefault();
        onSubmit();
      }}>
        <div className="flex flex-col sm:flex-row justify-between items-center">
          <div className="w-full sm:w-auto">
            <input className="" type="file" name="data" onChange={(e) => setFile(e.target.files[0])} />
          </div>
          <div className="flex items-center justify-end w-full">
            <div>
              <AdminSubmitButton title="Upload" />
            </div>
            {isLoading &&
              <div className="px-2">
                <SpinnerSVG />
              </div>
            }
          </div>
        </div>
        {error && (
          <div className="font-semibold text-rose-900">
            Error: {error}
          </div>
        )}
        {fileError && (
          <div className="font-semibold text-rose-900">
            File error: {fileError}
          </div>
        )}
        {dataError && (
          <div className="font-semibold text-rose-900">
            Data error: {dataError}
          </div>
        )}
        {message && (
          <div className="font-semibold text-green-500">
            {message}
          </div>
        )}
      </form>
    </Fragment>
  )
}

export default CSVUploader;