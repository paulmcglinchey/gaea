import { useState } from "react";

const CSVFieldButton = (props) => {

  const [showType, setShowType] = useState(props.showType);
  const toggleShowType = () => setShowType(!showType);

  const schemaItemColorPicker = (type) => {
    if (type === "Integer (11)") {
      return "bg-blue-500";
    } else if (type === "Varchar (255)") {
      return "bg-orange-600";
    } else if (type === "Varchar (2000)") {
      return "bg-red-600";
    } else if (type === "Varchar (1)") {
      return "bg-rose-400";
    } else {
      return "bg-yellow-500";
    }
  }

  return (
    <button onClick={() => toggleShowType()} className={`${schemaItemColorPicker(props.type)} font-bold p-1 rounded`}>
      {props.title}
      <span className={`${showType ? "inline-block" : "hidden"}`}> / <span className="font-thin">
        {props.type}
      </span>
      </span>
    </button>
  )
}

export default CSVFieldButton;