const FormInput = (props) => (

  <input
    className="rounded p-3 my-2 border-black border-4"
    onChange={(e) => props.onChange(e.target.value)}
    name={`${props.inputName}`}
    placeholder={`${props.inputName}`}
    value={props.value}
    type={props.type}
  />

)

export default FormInput;