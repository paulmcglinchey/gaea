const TogglerButton = (props) => {
  return (
    <button className="p-1 px-2 border-2 border-green-400 bg-white hover:bg-cyan-400 transition-all duration-600 font-bold rounded-sm" onClick={() => props.onclick()}>{props.title}</button>
  )
}

export default TogglerButton;