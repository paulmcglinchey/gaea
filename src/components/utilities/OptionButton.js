import { Fragment } from 'react';

const OptionButton = (props) => {

  const getColour = () => {
    if (props.type === "pro") {
      return "bg-blue-400";
    } else if (props.type === "con") {
      return "bg-rose-400";
    } else {
      return "bg-orange-500";
    }
  }

  return (
    <Fragment>
      <button className={`px-1 text-sm font-thin mx-1 rounded ${getColour()} text-black flex flex-shrink whitespace-nowrap m-1`}>
        {props.text}
      </button>
    </Fragment>
  )
}

export default OptionButton;