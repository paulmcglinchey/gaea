import { useState } from "react";
import AdminSubmitButton from "./utilities/AdminSubmitButton.js";
import DataSelector from './utilities/DataSelector.js';
import dataOptions from '../json/dataOptions.json';
import TestChart from '../charts/ScatterChart.js';
import endpoints from "../endpoints.js";
import InputError from "./utilities/InputError.js";
import inputStyling from './utilities/inputStyling.js';

const AddScatter = (props) => {

  const [selection1, setSelection1] = useState("UrbanMetric");
  const [selection2, setSelection2] = useState("CO2");
  const [label, setLabel] = useState(null);
  const [labelError, setLabelError] = useState(null);

  const [data, setData] = useState(null);
  const [isDataLoaded, setIsDataLoaded] = useState(false);

  const handleSubmit = () => {

    setLabelError(null);

    if (!label || label.trim().length === 0) {
      setLabelError("label cannot be empty");
      return;
    }

    fetch(`${endpoints.getGraphData}?selection1=${selection1}&selection2=${selection2}`, {
      method: 'GET'
    })
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result);
          setData(result.data);
          setIsDataLoaded(true);
        }
      )
      .catch(
        (error) => {
          console.log(error);
        }
      )
  }

  return (
    <form onSubmit={(e) => {
      e.preventDefault();
      handleSubmit();
    }}>
      <div className="flex flex-col sm:flex-row justify-between w-full sm:space-x-2 space-x-0">
        <input className={`flex-grow ${inputStyling}`} name="label" type="text" value={label} onChange={(e) => setLabel(e.target.value)} placeholder="label" />
        {labelError && <InputError error={labelError} />}
      </div>
      <DataSelector label="Selection 1" onChange={setSelection1} value={selection1}>
        {dataOptions.map((table, key) => {
          return (
            <optgroup key={key} label={table.table}>
              {table.items.scope.map((scope, key) => {
                return (
                  <option key={key} value={scope.query}>{scope.name}</option>
                )
              })}
            </optgroup>
          )
        })}
      </DataSelector>
      <DataSelector label="Selection 2" onChange={setSelection2} value={selection2}>
        {dataOptions.map((table, key) => {
          return (
            <optgroup key={key} label={table.table}>
              {table.items.scope.map((scope, key) => {
                return (
                  <option key={key} value={scope.query}>{scope.name}</option>
                )
              })}
            </optgroup>
          )
        })}
      </DataSelector>
      <div className="flex justify-end">
        <AdminSubmitButton title="Submit" />
      </div>
      {isDataLoaded &&
        <TestChart data={data} label={label} />
      }
    </form>
  )
}

export default AddScatter;