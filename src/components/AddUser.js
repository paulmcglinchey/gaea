import { Fragment, useState } from 'react';
import endpoints from '../endpoints.js';
import SpinnerSVG from './svg/SpinnerSVG.js';
import AdminSubmitButton from './utilities/AdminSubmitButton.js';
import inputStyling from './utilities/inputStyling.js';

const AddUser = (props) => {

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const [usernameError, setUsernameError] = useState(null);
  const [passwordError, setPasswordError] = useState(null);

  const [isLoading, setIsLoading] = useState(false);

  const onSubmit = () => {

    setIsLoading(true);

    setUsernameError(null)
    setPasswordError(null);

    if (username && username.trim().length === 0) {
      setUsernameError("username must have non-whitespace characters");
      setIsLoading(false);
      return;
    } else if (!username) {
      setUsernameError("username must not be null");
      setIsLoading(false);
      return;
    }

    if (password && password.trim().length === 0) {
      setPasswordError("password must have non-whitespace characters");
      setIsLoading(false);
      return;
    } else if (!password) {
      setPasswordError("password must not be null");
      setIsLoading(false);
      return;
    }


    fetch(endpoints.addUser, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        username,
        password,
        'admin': props.loggedInUser
      })
    })
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result.data);
          setIsLoading(false);
        }
      )
      .catch(
        (error) => {
          console.log(error);
          setIsLoading(false);
        })
  }

  return (
    <Fragment>
      <form onSubmit={(e) => {
        e.preventDefault();
        onSubmit();
      }}>
        <div className="flex flex-col">
          <div className="flex md:flex-row flex-col items-center flex-grow md:space-x-2 space-x-0 space-y-2 md:space-y-0">
            <input className={inputStyling} onChange={(e) => setUsername(e.target.value)} type="text" name="username" value={username} placeholder="username" />
            <input className={inputStyling} onChange={(e) => setPassword(e.target.value)} type="text" name="password" value={password} placeholder="password" />
            <div className="flex items-center w-full md:w-auto justify-end">
              {isLoading &&
                <div className="px-2 md:order-last order-first">
                  <SpinnerSVG />
                </div>
              }
              <AdminSubmitButton title="Add user" />
            </div>
          </div>
          <div className="flex items-center justify-center mt-2 text-red-600 font-bold">
            {usernameError && <div className="border-2 border-red-600 p-1 rounded-sm">{usernameError}</div>}
            {passwordError && <div className="border-2 border-red-600 p-1 rounded-sm">{passwordError}</div>}
          </div>
        </div>
      </form>
    </Fragment>
  )
}

export default AddUser;