import { useEffect, useState } from "react";
import endpoints from "../endpoints";
import QuitSVG from "./svg/QuitSVG";
import LineChart from '../charts/LineChart.js';

const AddLine = (props) => {

  const [hor, setHor] = useState('Year');
  const [verts, setVerts] = useState(['UrbanMetric']);

  const [labels, setLabels] = useState(null);
  const [datasets, setDatasets] = useState([]);

  const updateVerts = index => e => {
    const tempVerts = [...verts];
    tempVerts[index] = e.target.value;
    setVerts(tempVerts);
  }

  const addVert = () => {
    setVerts(verts => [...verts, 'UrbanMetric']);
  }

  const removeVert = () => {
    const tempVerts = [...verts];
    tempVerts.pop();
    setVerts(tempVerts);
  }

  const Selector = (props) => {
    return (
      <div className="flex p-1 justify-between rounded bg-emerald-400 m-1 md:m-2">
        <label className="font-semibold mr-1">{props.label}</label>
        {props.children}
      </div>
    )
  }

  const createStringQuery = () => {
    var tempQuery = `?hor=${hor}&verts=${verts.length}&`;
    for (var i = 0; i < verts.length; i++) {
      tempQuery += `selection${i}=${verts[i]}&`;
    }

    return tempQuery;
  }

  const colorPicker = [
    '#EC4899',
    '#4ADE80',
    '#22D3EE',
    '#F43F5E'
  ]

  const options = [
    { 'name': 'Urban Mileage', 'query': 'UrbanMetric' },
    { 'name': 'Extra Urban Mileage', 'query': 'ExtraUrbanMetric' },
    { 'name': 'Overall Mileage', 'query': 'CombinedMetric' },
    { 'name': 'Carbon Dioxide', 'query': 'CO2' },
    { 'name': 'Carbon Monoxide', 'query': 'CO' },
    { 'name': 'NoX', 'query': 'NOX' },
    { 'name': 'Noise Levels', 'query': 'NoiseLevel' },
    { 'name': 'Fuel Cost (12000 Miles)', 'query': 'FuelCost12000Miles' }
  ]

  useEffect(() => {

    setDatasets([]);
    setLabels(null);

    fetch(`${endpoints.getLineData}${createStringQuery()}`, {
      method: 'GET'
    })
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result);
          setLabels(result.labels);
          result.datasets.map((item, key) => {
            setDatasets(datasets => [...datasets, {
              label: item.name,
              data: item.data,
              fill: false,
              backgroundColor: colorPicker[key],
              borderColor: colorPicker[key],
              yAxisID: `y-axis-${key}`,
              tension: 0.2
            }]);
          })
        }
      )
      .catch(
        (error) => {
          console.log(error);
        }
      )
  }, [hor, verts])

  return (
    <div className="flex flex-col w-full">
      <form>
        <div className="flex flex-col md:flex-row flex-wrap justify-between">
          <Selector label="Horizontal Axis">
            <select className="rounded bg-green-100 flex-grow" value={hor} onChange={(e) => setHor(e.target.value)}>
              <option value="Year">Year</option>
            </select>
          </Selector>
          {verts.map((vert, key) => {
            return (
              <Selector label={`Vertical Axis ${key + 1}`}>
                <select className="rounded bg-green-100 flex-grow" key={key} value={vert} onChange={updateVerts(key)}>
                  {options.map((option, key) => {
                    return (
                      <option key={key} value={option.query}>{option.name}</option>
                    )
                  })}
                </select>
              </Selector>
            )
          })}
        </div>
        <div className="flex justify-end px-2 space-x-2 items-center">
          <button className="w-8 h-8 p-1" type="button" onClick={() => removeVert()}>
            <div className="transform hover:scale-90 transition-all duration-300 ease-in-out text-red-500">
              <QuitSVG />
            </div>
          </button>
          <button className="w-8 h-8 p-1" type="button" onClick={() => addVert()}>
            <div className="transform rotate-45 hover:scale-90 transition-all duration-300 ease-in-out text-green-500">
              <QuitSVG />
            </div>
          </button>
        </div>
      </form>
      {
        datasets &&
        <LineChart datasets={datasets} labels={labels} />
      }
    </div >
  )
}

export default AddLine;