import Content from './Content.js';
import AddLine from './AddLine.js';
import AddCategorical from './AddCategorical.js';

const Analytics = (props) => {

    const Container = (props) => {
        return (
            <div className="flex justify-center w-full md:p-5 p-2 bg-white shadow-2xl rounded-lg max-h-screen">{props.children}</div>
        )
    }

    return (
        <Content needsHero={true} setIsHeroBeingUsed={props.setIsHeroBeingUsed}>
            <div className="flex flex-col w-full space-y-4 mt-2">
                <Container><AddLine /></Container>
                <Container><AddCategorical /></Container>
                <Container></Container>
            </div>
        </Content>
    )
}

export default Analytics;