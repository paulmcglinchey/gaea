import { useEffect, useState } from "react";
import endpoints from "../endpoints";
import ChevronSVG from "./svg/ChevronSVG";
import SpinnerSVG from "./svg/SpinnerSVG";

const ActivityLog = props => {

  const [logItems, setLogItems] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [offset, setOffset] = useState(0);
  const [maxOffset, setMaxOffset] = useState(0);

  const pageBack = () => {
    const tempOffset = offset;
    if ((tempOffset - 10) <= 0) {
      setOffset(0);
    } else {
      setOffset(tempOffset - 10);
    }
    console.log(offset);
  }

  const pageForward = () => {
    const tempOffset = offset;
    if ((tempOffset + 10) >= maxOffset) {
      setOffset(maxOffset);
    } else {
      setOffset(tempOffset + 10);
    }
    console.log(offset);
  }

  useEffect(() => {

    setIsLoading(true);

    fetch(`${endpoints.getLog}?offset=${offset}`, {
      method: 'GET'
    })
      .then(res => res.json())
      .then(
        (result) => {
          setIsLoading(false);
          setLogItems(result.data);
          setMaxOffset(result.maxOffset);
        }
      )
      .catch(
        (error) => {
          setIsLoading(false);
          console.log(error);
        }
      )
  }, [offset])

  return (
    <div>
      {logItems &&
        <div>
          {logItems.map((item, key) => {
            return (
              <div key={key} className="flex flex-col sm:flex-row justify-start sm:justify-between items-center p-2 my-2 rounded shadow-2xl bg-white">
                <div className="font-medium select-none">
                  <span className="text-lightBlue-900 hover:text-lightBlue-700">{item.username}</span>
                  <span className="text-black"> {item.Activity} </span>
                  <span className="text-orange-500">{item.Item && item.Item}</span>
                </div>
                <div className="font-light text-sm">{item.Date}</div>
              </div>
            )
          })}
          <div className="flex w-full justify-end items-center space-x-2">
            <div className="p-2">
              {(offset / 10) + 1} of {(maxOffset / 10) + 1}
            </div>
            <button className="p-2 rounded transform -rotate-90 hover:scale-110 transition-all duration-600" onClick={(e) => pageBack()} type="button">
              <ChevronSVG width="w-6" height="h-6" />
            </button>
            <button className="p-2 rounded transform rotate-90 hover:scale-110 transition-all duration-600" onClick={(e) => pageForward()} type="button">
              <ChevronSVG width="w-6" height="h-6" />
            </button>
          </div>
        </div>
      }
      {isLoading &&
        <div className="w-6 h-6">
          <SpinnerSVG />
        </div>
      }
    </div >
  )
}

export default ActivityLog;