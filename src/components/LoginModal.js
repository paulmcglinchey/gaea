import { Fragment, useState } from 'react';
import { Redirect } from 'react-router-dom';
import FormInput from './utilities/FormInput.js';
import SpinnerSVG from './svg/SpinnerSVG.js';
import QuitSVG from './svg/QuitSVG.js';
import endpoints from '../endpoints.js';

const LoginModal = (props) => {

    const [username, setUsername] = useState(null);
    const [password, setPassword] = useState(null);
    const [usernameError, setUsernameError] = useState(null);
    const [passwordError, setPasswordError] = useState(null);

    const [isLoginLoading, setIsLoginLoading] = useState(false);
    const [loginStatus, setLoginStatus] = useState(null);

    const onSubmit = () => {
        setIsLoginLoading(true);
        setUsernameError(null);
        setPasswordError(null);

        if (!username) {
            setUsernameError("username cannot be null");
            setIsLoginLoading(false);
            return;
        } else {
            if (username.trim().length === 0) {
                setUsernameError("username must contain actual characters");
                setIsLoginLoading(false);
                return;
            }
        }
        if (!password) {
            setPasswordError("password cannot be null");
            setIsLoginLoading(false);
            return;
        } else {
            if (password.trim().length === 0) {
                setPasswordError("password must contain actual characters");
                setIsLoginLoading(false);
                return;
            }
        }

        // data is acceptable, making the request to the server
        fetch(endpoints.authenticate, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username,
                password
            })
        })
            .then(res => res.json())
            .then(
                (result) => {
                    props.setUserAuth(result.isUserAuthenticated);
                    if (result.isUserAuthenticated) {
                        props.setLoggedInUser(result.username);
                        console.log("logged in user:", result.username);
                        setLoginStatus("success");
                    } else {
                        setLoginStatus("failed");
                    }
                    setIsLoginLoading(false);
                }
            )
    }

    if (props.isOpen) {
        if (props.isUserAuth) {
            props.toggleLoginModal();
            return (
                <Redirect to="/admin" />
            )
        } else {
            return (
                <Fragment>
                    <div className="absolute top-0 w-screen h-screen bg-gray-500 bg-opacity-30 backdrop-filter backdrop-blur-md transition-all">
                        <div className="absolute top-0 md:top-1/4 left-1/2 transform -translate-x-1/2 w-full md:w-2/3 lg:w-1/2 h-auto bg-white text-black z-50 rounded p-3">
                            {loginStatus && loginStatus === "failed" &&
                                <div className="flex bg-red-400 rounded-lg p-2 mb-2 text-white items-center">
                                    <div className="flex-grow">Login failed</div>
                                    <div className="">
                                        <button className="h-4 w-4" onClick={() => setLoginStatus(null)}>
                                            <QuitSVG />
                                        </button>
                                    </div>
                                </div>
                            }
                            <div className="flex items-center">
                                <h1 className="flex-grow text-2xl font-bold">
                                    Sign in for admin access
                                </h1>
                                <div className="inline-block float-right h-full">
                                    <button className="h-4 w-4 text-red-600 rounded-full inline-block" onClick={() => props.toggleLoginModal()}>
                                        <QuitSVG />
                                    </button>
                                </div>
                            </div>
                            <hr />
                            <form onSubmit={(e) => {
                                e.preventDefault();
                                onSubmit()
                            }}>
                                <div className="flex flex-col">
                                    <FormInput inputName="username" type="text" onChange={setUsername} value={username} />
                                    {usernameError &&
                                        <span className="text-red-700">{usernameError}</span>
                                    }
                                    <FormInput inputName="password" type="password" onChange={setPassword} value={password} />
                                    {passwordError &&
                                        <span className="text-red-700">{passwordError}</span>
                                    }
                                    <button className={`flex p-3 border-2 justify-center space-x-2 border-springgreen hover:bg-springgreen rounded font-bold text-md mt-5 ${isLoginLoading ? "animate-pulse" : ""}`} type="submit" name="submit" value="Log in">
                                        <div className="">Log in</div>
                                        <div className="h-6 w-6">
                                            {isLoginLoading && <SpinnerSVG />}
                                        </div>
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </Fragment >
            )
        }
    } else {
        return null;
    }

}

export default LoginModal;