const QuitSVG = (props) => {
  return (
    <svg viewBox="0 0 48 48" className={`stroke-current ${props.classes}`}>
      <path
        stroke-width="8"
        fill="none"
        stroke-linecap="round"
        d="M 46.26831,46.26831 24,24 1.7316901,46.26831 m 0,-44.5366199 L 24,24 46.26831,1.7316901" />
    </svg >
  )
}

export default QuitSVG;