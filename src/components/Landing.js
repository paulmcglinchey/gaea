import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import endpoints from '../endpoints.js';
import Content from './Content.js';
import SpinnerSVG from './svg/SpinnerSVG.js';

const Landing = (props) => {

    const [items, setItems] = useState(null);
    const [isLoading, setIsLoading] = useState(false);

    useEffect(() => {
        setIsLoading(true);

        fetch(`${endpoints.getContent}?needImages=1&typeContent=gaea_landing`, {
            method: 'GET'
        })
            .then(res => res.json())
            .then(
                (result) => {
                    setItems(result.data);
                    setIsLoading(false);
                }
            )
            .catch(
                (error) => {
                    console.log(error);
                    setIsLoading(false);
                }
            )
    }, [])

    return (
        <Content needsHero={true} setIsHeroBeingUsed={props.setIsHeroBeingUsed}>
            {items &&
                <div>
                    {items.map((item, key) => {
                        return (
                            <Link to={`/landingArticle/${item.id}`}>
                                <div style={{ backgroundImage: `url("${item.url}")` }} key={key} className="flex mt-5 mb-10 flex-col bg-pink-100 rounded-lg shadow-2xl transform hover:scale-95 transition-all duration-300 ease-in-out bg-center bg-cover">
                                    <div className="px-5 md:px-24 py-5 md:py-28 rounded-lg backdrop-filter backdrop-blur-sm backdrop-brightness-75">
                                        <div className="flex-col">
                                            <div className="flex-grow mb-8">
                                                <h1 className="inline-block text-2xl md:text-5xl font-bold text-white">{item.title}</h1>
                                            </div>
                                            <div>
                                                <h3 className="text-lg md:text-2xl font-semibold text-white">{item.summary}</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Link>
                        )
                    })}
                </div>
            }
            {isLoading &&
                <div className="flex h-24 justify-center">
                    <SpinnerSVG />
                </div>
            }
        </Content>
    )
}

export default Landing;