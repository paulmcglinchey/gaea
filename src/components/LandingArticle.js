import { useEffect, useState } from "react";
import { useParams } from "react-router";
import endpoints from "../endpoints.js";
import Content from './Content.js';
import SpinnerSVG from "./svg/SpinnerSVG.js";
import ReactMarkdown from 'react-markdown';
import RemarkBreaks from 'remark-breaks';

const LandingArticle = props => {

  const [article, setArticle] = useState(null);
  const [isLoading, setIsLoading] = useState(false);

  let { id } = useParams();

  useEffect(() => {
    setIsLoading(true);

    fetch(`${endpoints.getArticle}?articleID=${id}&typeContent=gaea_landing`, {
      method: 'GET'
    })
      .then(res => res.json())
      .then(
        (result) => {
          console.log(result);
          setArticle(result.data);
          setIsLoading(false);
        }
      )
      .catch(
        (error) => {
          console.log(error);
          setIsLoading(error);
        }
      )

  }, [id])

  return (
    <Content needsHero={true} setIsHeroBeingUsed={props.setIsHeroBeingUsed}>
      {isLoading &&
        <div className="flex w-96 justify-center h-96">
          <div className="">
            <SpinnerSVG />
          </div>
        </div>
      }
      {article &&
        <div className="flex flex-col lg:flex-row mt-5 space-y-4 lg:space-y-0 space-x-0 lg:space-x-4 mb-10">
          <div style={{ backgroundImage: `url("${article.URL}")` }} className="lg:flex-1 flex items-end justify-center rounded-lg h-96 w-full lg:w-auto lg:h-auto bg-cover bg-center shadow-2xl transform hover:scale-105 duration-300 ease-in-out">
            <div className="flex items-center backdrop-filter backdrop-brightness-50 h-full w-full rounded-lg pb-3">
              <h1 className="font-bold text-6xl text-springgreen text-center antialiased px-3">{article.Title}</h1>
            </div>
          </div>
          <div className="flex-1 flex-col">
            <div className="text-justify space-y-4"><ReactMarkdown remarkPlugins={[RemarkBreaks]}>{article.Content}</ReactMarkdown></div>
            <div className="text-right italic text-sm font-light">authored by {article.username}</div>
          </div>
        </div>
      }
    </Content>
  )
}

export default LandingArticle;