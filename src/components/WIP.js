const WIP = (props) => {
  return (
    <div className="flex justify-center items-center p-10 bg-gradient-to-b from-orange-500 to-pink-500 rounded-lg">
      <div className="h-32 w-full relative">
        <div className="absolute inset-0 bg-white opacity-25 rounded-lg shadow-2xl"></div>
        <div className="h-full w-full transition transform duration-300 hover:scale-105">
          <div className="h-full w-full bg-white rounded-lg shadow-2xl flex items-center justify-center">
            <h1 className="font-bold text-4xl text-center">Work in progress</h1>
          </div>
        </div>
      </div>
    </div>
  )
}

export default WIP;