import { useState, Fragment } from 'react';
import NavbarBrand from './NavbarBrand.js';
import NavCollapser from './NavCollapser.js';
import NavToggler from './NavToggler.js';
import NavLogin from './NavLogin.js';
import MobileNavBackground from './MobileNavBackground.js';

const NavHeader = (props) => {

    const [isNavOpen, setIsNavOpen] = useState(false);
    const toggleNav = () => setIsNavOpen(!isNavOpen);

    const colorSelector = () => {
        if (props.isHeroBeingUsed) {
            return "text-white";
        } else {
            if (isNavOpen) {
                return "text-white md:text-black";
            } else {
                return "text-black";
            }
        }
    }

    return (
        <Fragment>
            {!props.isLoginModalOpen &&
                <div className={`w-full content-center mx-auto relative z-20 px-2 md:px-5 ${colorSelector()} container`}>
                    <div id="mx-auto">
                        <div className=" flex flex-wrap xl:px-0 justify-end items-center overflow-hidden" id="navbar">
                            <NavbarBrand isOpen={isNavOpen} toggler={toggleNav} />
                            <NavCollapser isOpen={isNavOpen} toggler={toggleNav} isHeroBeingUsed={props.isHeroBeingUsed} />
                            <NavToggler toggler={toggleNav} isOpen={isNavOpen} />
                            <NavLogin toggleLoginModal={props.toggleLoginModal} isUserAuthenticated={props.isUserAuthenticated} />
                            <MobileNavBackground isOpen={isNavOpen} />
                        </div>
                    </div>
                </div>
            }
        </Fragment>
    )
}

export default NavHeader;